import sys
import shutil
import os
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'
from os import listdir
from os.path import isfile, join
import numpy
import NDCG
import RBO
from bisect import bisect
from random import random
from nltk.translate import bleu_score
from nltk.translate.bleu_score import SmoothingFunction
import numpy as np
import tensorflow as tf
from tensorflow.contrib import rnn
import random
from random import random
import collections
import time
import codecs
import json
import rougescore
import math
from sklearn.cluster import KMeans




def isEnglish(s):
    try:
        codecs.encode(s,'ascii')
    except:
        return False
    else:
        return True

    
DataDirectory='/home/t-shsant/Data/OrganisedData/'
embedFile = '../Data/glove.6B.100d.txt'
Unknown_Word_Embeddings_File = '../Saved_Models/Unknown_Word_Embeddings.txt'


Stopwords=[]


with open('stop-word-list.txt','r')as inputfile:
    for line in inputfile:
        Stopwords.append(line.strip().lower())
        

def remove_stopwords(sentence):
    cleanSentence=[]
    for word in sentence:
        if word not in Stopwords:
            cleanSentence.append(word)
    return cleanSentence


'''
class ToySequenceData(object):
    
    def __init__(self, raw_data, max_seq_len=20):
        
        self.data = []
        self.labels = []
        self.seqlen = []
                
        for i in range(len(raw_data)):
            for j in range(2,len(raw_data[i])):
                self.seqlen.append(j)
                s=[]
                for k in range(j):
                    s.append([float(dictionary[raw_data[i][k]])])
                s+= [[0.0] for i in range(max_seq_len - j)]
                self.data.append(s)
                symbols_out_onehot = np.zeros([vocab_size], dtype=float)
                symbols_out_onehot[dictionary[raw_data[i][j]]] = 1.0
                self.labels.append(symbols_out_onehot)
            self.batch_id = 0



                                              
    def next(self, batch_size):                                                       
        if self.batch_id == len(self.data):
            self.batch_id = 0
        batch_data = (self.data[self.batch_id:min(self.batch_id +
                                                  batch_size, len(self.data))])
        batch_labels = (self.labels[self.batch_id:min(self.batch_id +
                                                  batch_size, len(self.data))])
        batch_seqlen = (self.seqlen[self.batch_id:min(self.batch_id +
                                                  batch_size, len(self.data))])
        self.batch_id = min(self.batch_id + batch_size, len(self.data))
                                              
        return batch_data, batch_labels, batch_seqlen,self.batch_id

    def setBatchIDtoZero():
        self.batch_id = 0
'''      



def loadGloVe(embedFile,dictionary,reverse_dictionary):
    embeddingsDict={}
    
    file = open(embedFile,'r', encoding="utf8")
    print('Loading Embeddings!')
    
    for line in file.readlines():
        if line.split()[0] in dictionary:
            row = line.strip().split(' ')
            embeddingsDict[row[0]]=np.asarray(row[1:])
            #vocab.append(row[0])
            #embd.append(row[1:])
    file.close()

    file = open(Unknown_Word_Embeddings_File,'r', encoding="utf8")
    print('Loading Embeddings!')
    
    for line in file.readlines():
        if line.split()[0] in dictionary:
            row = line.strip().split(' ')
            embeddingsDict[row[0]]=np.asarray(row[1:])
    file.close()

    embeddingsArray=[]
    for wordid in sorted(reverse_dictionary):
        embeddingsArray.append(embeddingsDict[reverse_dictionary[wordid]])
        #if reverse_dictionary[wordid] not in embeddingsDict:
            #embeddingsDict[reverse_dictionary[wordid]]=np.asarray([0.0 for i in range(len(row[1:]))])
          
    print('Embeddings Loaded!')
    
    return embeddingsDict,embeddingsArray



def sample(preds, temperature=1.0):
    # helper function to sample an index from a probability array
    preds = np.asarray(preds).astype('float64')
    preds = np.log(preds) / temperature
    exp_preds = np.exp(preds)
    preds = exp_preds / np.sum(exp_preds)
    probas = np.random.multinomial(1, preds, 1)
    return np.argmax(probas)



def MySoftMax(preds):
    preds = np.asarray(preds).astype('float64')
    #preds = np.log(preds)
    exp_preds = np.exp(preds)
    preds = exp_preds / np.sum(exp_preds)
    #probas = np.random.multinomial(1, preds, 1)
    return preds



areas=['.']
r=4

YearToi={}
Conference={}
iToYear={}



with open('../Saved_Models/dictionary.json', 'r') as fp:
    dictionary=json.load(fp)
with open('../Saved_Models/reverse_dictionary.json', 'r') as fp:
    reverse_dictionary=json.load(fp)

'''BackGroundModel=[]
with open('../Saved_Models/BM.txt','r') as inputfile:
    for number in inputfile.readline().split(','):
        BackGroundModel.append(float(number))'''



                
for area in sorted(areas):
    confs = [f for f in listdir(DataDirectory+area) if isfile(join(DataDirectory+area, f))]
    Conference[area]={}
    ActualTitles=[]
    paperCount=0
    maxSeqlength=0

    AllWords=[]
    BagOfWords={}
    WordCount={}

    ConfArray=[]
    ConfDistribution={}


    
    for confname in sorted(confs):
        conf=confname.replace('.txt','')
        ConfArray.append(conf)
        with open(DataDirectory+area+'/'+conf+'.txt','r') as inputfile:
            for line in inputfile:
                year=int(line.split('\t')[6])

                if year < 1990:
                    continue
                InitialTitle=line.strip().split('\t')[4]

                if isEnglish(InitialTitle)==False:
                    continue

                Title='#'

                for word in InitialTitle.split():
                    if word in dictionary:
                        Title+=' '+word
                    else:
                        Title+=' <unk_'+conf+'>'

                Title+=' .'


                if year not in BagOfWords:
                    BagOfWords[year]={}

                if conf not in BagOfWords[year]:
                    BagOfWords[year][conf]={}

                for word in Title.split()[1:]:
                    if word in BagOfWords[year][conf]:
                        BagOfWords[year][conf][word]+=1
                    else:
                        BagOfWords[year][conf][word]=1

                if year > 2016 or year< 2000:
                    continue

                if year not in Conference[area]:
                    Conference[area][year]=[]

                Conference[area][year].append(Title.split())

                if len(InitialTitle.split())+1 > maxSeqlength:
                    maxSeqlength=len(Title.split())+1

                paperCount+=1



    ConferenceOneHotVector={}
    for confname in sorted(confs):
        conf=confname.replace('.txt','')
        ConferenceOneHotVector[conf]=numpy.zeros(len(ConfArray))
        ConferenceOneHotVector[conf][ConfArray.index(conf)]=1.0
    

    with tf.Session() as sess:   
        saver = tf.train.import_meta_graph('../Saved_Models/model.ckpt.meta')
        saver.restore(sess,tf.train.latest_checkpoint('../Saved_Models/'))


        vocab_size=sess.run('vocab_size:0')
        maxSeqlength=sess.run('maxSeqlength:0')
        #n_hidden=sess.run('n_hidden:0')



        graph=tf.get_default_graph()

        x = graph.get_tensor_by_name("x:0")
        y = graph.get_tensor_by_name("y:0")
        BM = graph.get_tensor_by_name("BM:0")
        seqlen = graph.get_tensor_by_name("seqlen:0")
        seqyear = graph.get_tensor_by_name("seqyear:0")
        seqBagofWords = graph.get_tensor_by_name("seqBagofWords:0")  
        
        '''with open('variables.txt','w')as outputfile:
            for i in graph.get_operations():
                outputfile.write(i.name+'\n')'''

                
        pred = graph.get_tensor_by_name("pred:0")
        cost = graph.get_tensor_by_name("cost:0")
        TopicNet = graph.get_tensor_by_name("TopicNet:0")
        


        




        
        embeddingsDict,embeddingsArray=loadGloVe(embedFile,dictionary,reverse_dictionary)
        embedding_dim = len(embeddingsArray[0])



        HistoryData=[]

        i=0 
        for year in sorted(BagOfWords):
            for conf in ConfArray:
                YearToi[year]=i
                iToYear[i]=year
                HistoryData.append([])
                for index in range(1,r+1):
                    if i-index < 0:
                        for j in range(vocab_size):
                            HistoryData[i].append(0.0)
                    else:
                        if conf not in BagOfWords[iToYear[i-index]]:
                            for word in sorted(dictionary,key=dictionary.get):
                                HistoryData[i].append(0.0)
                        else:
                            for word in sorted(dictionary,key=dictionary.get):
                                if word in BagOfWords[iToYear[i-index]][conf]:
                                    HistoryData[i].append(float(BagOfWords[iToYear[i-index]][conf][word]))
                                else:
                                    HistoryData[i].append(0.0)
                i+=1

        #print((HistoryData[5][:30]))

        #print(YearToi[2015])
        
        for i in range(len(HistoryData)):
            for index in range(0,r):
                MySum=sum(HistoryData[i][vocab_size*index : vocab_size*(index+1)])
                if MySum==0.0:
                    continue
                for j in range(0,vocab_size):
                    HistoryData[i][vocab_size*index+j]=HistoryData[i][vocab_size*index+j]/MySum


        
        TopicModel=[]

        for word in sorted(dictionary, key=dictionary.get):
            frCount=np.zeros([vocab_size], dtype=float)
            frCount[dictionary[word]]=1.0
            TopicModel.append(sess.run(TopicNet,feed_dict={seqBagofWords: [frCount], seqyear: [YearToi[2015]]}).flatten().tolist())

        #print(len(TopicModel),len(TopicModel[0]))
        TopicModel=numpy.asarray(TopicModel)

        kmeans = KMeans(n_clusters=50, random_state=15845).fit(TopicModel)

        Clusters={}
        Similarity={}

        i=0
        for topic in kmeans.labels_:
            if topic not in Clusters:
                Clusters[topic]=[]
            Clusters[topic].append(reverse_dictionary[str(i)])
            i+=1
            
        
        
                    
        IgnoreList=Stopwords+['<unk_ML>', '#', '.']

        topic=0
        for center in kmeans.cluster_centers_:
            Similarity[topic]={}
            for word in Clusters[topic]:
                Similarity[topic][word]=numpy.linalg.norm(TopicModel[dictionary[word]]-center)
            topic+=1

        topic=0
        with open('TopicModel.txt','w') as outputfile:
            for topic in Similarity:
                outputfile.write('Topic No: '+str(topic+1)+'\n-------------------------------\n')
                i=0
                for word in sorted(Similarity[topic],key=Similarity[topic].get)[:30]:
                    outputfile.write(word+' : '+str(Similarity[topic][word])+'\n')
                outputfile.write('\n\n\n')
                topic+=1


        break
        TopicModel=TopicModel.transpose().tolist()
        

        
        with open('TopicModel.txt','w') as outputfile:
            '''outputfile.write('BackGround Topic\n-------------------------------\n')
            for j in sorted(range(len(BackGroundModel)), key=lambda k: BackGroundModel[k], reverse=True)[:20]:
                outputfile.write(reverse_dictionary[str(j)]+': '+str(j)+': '+ str(BackGroundModel[j])+'\n')
                
            outputfile.write('\n\n\n\n')'''

            for i in range(len(TopicModel)):
                outputfile.write('Topic No: '+str(i+1)+'\n-------------------------------\n')
                for j in sorted(range(len(TopicModel[i])), key=lambda k: TopicModel[i][k])[:30]:
                    outputfile.write(reverse_dictionary[str(j)]+': '+str(TopicModel[i][j])+'\n')
                outputfile.write('\n\n\n\n')


        
        for confname in confs:
            Perplexity={}
            conf=confname.replace('.txt','')

            
            for year in range(2016,2017):
                RefereneTitles=Conference[area][year]
                individualYearPerplexity=[]

                pp=0.0
                wordCount=0
                
                
                for i in range(len(RefereneTitles)):
                    sentence=[]
                    sentence.append(dictionary['#'])
                    sentence+=[dictionary['$emp$'] for Myi in range(maxSeqlength - 1)]

                    s='#'
                    for j in range(1,len(RefereneTitles[i])):
                        wordCount+=1
                        frCount=np.zeros([vocab_size], dtype=float)
                        Mycount = collections.Counter(s.split()[0:j]).most_common()

                        for word,wordcount in Mycount:
                            frCount[dictionary[word]]=float(wordcount)
                            MySum=sum(frCount)
                            
                        if MySum!=0:
                            frCount=frCount/MySum
                    
                        onehot_pred = sess.run(pred, feed_dict={x: [sentence], seqlen: [j], seqyear: [YearToi[year]], seqBagofWords: [frCount] })
                        #onehot_pred=(onehot_pred[0]-min(onehot_pred[0]))/(max(onehot_pred[0])-min(onehot_pred[0]))
                        #loss = sess.run(cost, feed_dict={x: [sentence], seqlen: [j], seqyear: [YearToi[year]], seqBagofWords: [frCount]})

                        #print(min(onehot_pred[0]),max(onehot_pred[0]),numpy.mean(onehot_pred[0]))

                        Minimum=min(onehot_pred[0])
                        mysum=0.0
                        if Minimum<0:
                            for k in range(len(onehot_pred[0])):
                                onehot_pred[0][k]+=-Minimum
                                mysum+=onehot_pred[0][k]
                        k=0
                        multinomial=[]
                        for k in range(len(onehot_pred[0])):
                            multinomial.append(onehot_pred[0][k])
                            k+=1

                        
                        onehot_pred=MySoftMax(multinomial)
                        
                        
                        pp+=-math.log(onehot_pred[dictionary[RefereneTitles[i][j]]],2)

                        
                        sentence[j]=dictionary[RefereneTitles[i][j]]

                        s+=' '+RefereneTitles[i][j]
                        if RefereneTitles[i][j]=='.':
                            break
                        #print(RefereneTitles[i][j])
                    if i %100==0:
                        print(year,i,math.pow(2,pp/float(wordCount)))
                        
                Perplexity[year]=math.pow(2,pp/float(wordCount))

        print('\n\n\n\nPrinting the final perplexity scores\n#################\n\n')
        for year in sorted(Perplexity):
            print(year,Perplexity[year])

        print('\n\n\n\n')
        
            
                
        predictedNumberofPaper=200

        print('Predicted Number of Papers: '+str(predictedNumberofPaper))

        with open('output.txt','w') as outputfile:
            for confname in confs:
                BLEUscores=[]
                print('\n\n\nProducing papers for Conference: '+confname+'\n-----------------------------------------\n')
                outputfile.write('\n\n\nProducing papers for Conference: '+confname+'\n-----------------------------------------\n')
                
                conf=confname.replace('.txt','')

                for year in range(2000,2017):
                    RefereneTitles=[]
                    PredictedTitles=[]
                    IndividualBLEU={}
                    PairWiseBLEU={}
                    MostSimilarTitle={}
                    
                    
                    RefereneTitles=Conference[area][year]

                    
                    for i in range(predictedNumberofPaper):
                        while(True):
                            sentence=[]
                            sentence.append([])
                            sentence[0].append(dictionary['#'])
                            #sentence[0].append(dictionary['Conf_'+conf+':'])
                            #sentence[0]+=[[0.0 for m in range(embedding_dim)] for j in range(maxSeqlength - 2)]
                            sentence[0]+=[dictionary['$emp$'] for Myi in range(maxSeqlength - 1)]
                            s='# '

                            temparatue=0.04
                            
                            for j in range(1,maxSeqlength):
                                #print('shape:' +str(np.shape(sentence)))

                                frCount=np.zeros([vocab_size], dtype=float)
                                Mycount = collections.Counter(s.split()[0:j]).most_common()
                                for word,wordcount in Mycount:
                                    frCount[dictionary[word]]=float(wordcount)

                                MySum=sum(frCount)
                                if MySum!=0:
                                    frCount=frCount/MySum
                        
                                onehot_pred = sess.run(pred, feed_dict={x: sentence, seqlen: [j], seqyear: [YearToi[year]], seqBagofWords: [frCount] })
                                


                                
                                Minimum=min(onehot_pred[0])
                                mysum=0.0
                                if Minimum<0:
                                    for k in range(len(onehot_pred[0])):
                                        onehot_pred[0][k]+=-Minimum+1
                                        mysum+=onehot_pred[0][k]
                                k=0
                                multinomial=[]
                                for k in range(len(onehot_pred[0])):
                                    multinomial.append(onehot_pred[0][k])
                                    k+=1

                                for number in multinomial:
                                    if number <=0:
                                        print(number)

                                onehot_pred_index=sample(multinomial,temparatue)
                                temparatue=max(0.01,temparatue/2)


                                '''cdf = [multinomial[0]]
                                for k in range(1, len(multinomial)):
                                    cdf.append(cdf[-1] + multinomial[k])
                                onehot_pred_index = bisect(cdf,random())'''

                                #onehot_pred_index = (int(tf.argmax(onehot_pred, 1).eval()))
                                
                                
                                #sentence[0][j]=embeddingsDict[reverse_dictionary[str(onehot_pred_index)]]
                                sentence[0][j]=dictionary[reverse_dictionary[str(onehot_pred_index)]]
                                #print(reverse_dictionary[onehot_pred_index])
                                s+=reverse_dictionary[str(onehot_pred_index)]+' '
                                if reverse_dictionary[str(onehot_pred_index)]=='.':
                                    break
                            if j>=1:
                                break
                        outputfile.write(str(i+1)+' '+ str(len(s.split()))+' '+s+'\n\n')
                        #print('iteration: '+str(iteration)+'\tgenerated sentence: '+str(i+1))
                        #print(str(year)+': '+str(i+1)+': '+ s)
                        PredictedTitles.append(s.split())


                        
                        chencherry = SmoothingFunction()
                        #IndividualBLEU[i]=rougescore.rouge_l(s.split(),RefereneTitles[0],0.5)
                        PairWiseBLEU[i]={}
                        for j in range(len(RefereneTitles)):
                            PairWiseBLEU[i][j]=bleu_score.sentence_bleu([RefereneTitles[j][1:-1]],s.split()[1:-1],weights=(0.25, 0.25, 0.25, 0.25),smoothing_function=chencherry.method2)
                            #PairWiseBLEU[i][j]=float(bleu_score.modified_precision([RefereneTitles[0][j]],s.split()[1:-1],3))
                            #PairWiseBLEU[i][j]=rougescore.rouge_l(s.split()[1:-1],[RefereneTitles[j][1:-1]],0.50)
                        MostSimilarTitleNo=sorted(PairWiseBLEU[i],key=PairWiseBLEU[i].get, reverse=True)[0]
                        IndividualBLEU[i]=PairWiseBLEU[i][MostSimilarTitleNo]
                        MostSimilarTitle[i]=RefereneTitles[MostSimilarTitleNo]
                        del RefereneTitles[MostSimilarTitleNo]


                    #for titleNo in sorted(IndividualBLEU,key=IndividualBLEU.get, reverse=True)[:5]:
                        #print(str(IndividualBLEU[titleNo])+'\n'+' '.join(PredictedTitles[titleNo])+'\n\n'+' '.join(MostSimilarTitle[titleNo])+'\n---------------------------------\n\n')

                    mybleu=sum(IndividualBLEU.values())/len(IndividualBLEU)
                    BLEUscores.append(mybleu)
                    print('year: '+str(year)+'\tBlue Score: '+str(mybleu))
                    
                    
                print(numpy.average(BLEUscores),numpy.std(BLEUscores))




        



                    
