import sys
import shutil
import os
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'
from os import listdir
from os.path import isfile, join
import numpy
import NDCG
import RBO
from bisect import bisect
from random import random
from nltk.translate import bleu_score
from nltk.translate.bleu_score import SmoothingFunction
import numpy as np
import tensorflow as tf
from tensorflow.contrib import rnn
import random
from random import random
import collections
import time
import codecs
import json
import rougescore





def isEnglish(s):
    try:
        codecs.encode(s,'ascii')
    except:
        return False
    else:
        return True

    
DataDirectory='/home/t-shsant/Data/OrganisedData/'
embedFile = '../Data/glove.6B.100d.txt'

Stopwords=[]


with open('stop-word-list.txt','r')as inputfile:
    for line in inputfile:
        Stopwords.append(line.strip().lower())
        

def remove_stopwords(sentence):
    cleanSentence=[]
    for word in sentence:
        if word not in Stopwords:
            cleanSentence.append(word)
    return cleanSentence



class ToySequenceData(object):
    
    def __init__(self, raw_data, max_seq_len=20):
        
        self.data = []
        self.labels = []
        self.seqlen = []
                
        for i in range(len(raw_data)):
            for j in range(2,len(raw_data[i])):
                self.seqlen.append(j)
                s=[]
                for k in range(j):
                    s.append([float(dictionary[raw_data[i][k]])])
                s+= [[0.0] for i in range(max_seq_len - j)]
                self.data.append(s)
                symbols_out_onehot = np.zeros([vocab_size], dtype=float)
                symbols_out_onehot[dictionary[raw_data[i][j]]] = 1.0
                self.labels.append(symbols_out_onehot)
            self.batch_id = 0



                                              
    def next(self, batch_size):                                                       
        if self.batch_id == len(self.data):
            self.batch_id = 0
        batch_data = (self.data[self.batch_id:min(self.batch_id +
                                                  batch_size, len(self.data))])
        batch_labels = (self.labels[self.batch_id:min(self.batch_id +
                                                  batch_size, len(self.data))])
        batch_seqlen = (self.seqlen[self.batch_id:min(self.batch_id +
                                                  batch_size, len(self.data))])
        self.batch_id = min(self.batch_id + batch_size, len(self.data))
                                              
        return batch_data, batch_labels, batch_seqlen,self.batch_id

    def setBatchIDtoZero():
        self.batch_id = 0
        



def loadGloVe(embedFile,dictionary,reverse_dictionary):
    embeddingsDict={}
    
    file = open(embedFile,'r', encoding="utf8")
    print('Loading Embeddings!')
    
    for line in file.readlines():
        if line.split()[0] in dictionary:
            row = line.strip().split(' ')
            embeddingsDict[row[0]]=np.asarray(row[1:])
            #vocab.append(row[0])
            #embd.append(row[1:])
    file.close()

    embeddingsArray=[]
    for wordid in sorted(reverse_dictionary):
        if reverse_dictionary[wordid] not in embeddingsDict:
            embeddingsDict[reverse_dictionary[wordid]]=np.asarray([0.0 for i in range(len(row[1:]))])
        embeddingsArray.append(embeddingsDict[reverse_dictionary[wordid]])
            
    print('Embeddings Loaded!')
    
    return embeddingsDict,embeddingsArray



def sample(preds, temperature=1.0):
    # helper function to sample an index from a probability array
    preds = np.asarray(preds).astype('float64')
    preds = np.log(preds) / temperature
    exp_preds = np.exp(preds)
    preds = exp_preds / np.sum(exp_preds)
    probas = np.random.multinomial(1, preds, 1)
    return np.argmax(probas)



areas=['ML']

Conference={}
for area in sorted(areas):
    #confs = [f for f in listdir(DataDirectory+area) if isfile(join(DataDirectory+area, f))]
    confs=['kdd']
    Conference[area]={}

    for confname in confs:
        ActualTitles=[]
        conf=confname.replace('.txt','')
        Conference[area][conf]=[]

        paperCount=0
        maxSeqlength=0

        AllWords=[]
        
        with open(DataDirectory+area+'/'+conf+'.txt','r') as inputfile:
            for line in inputfile:
                year=int(line.split('\t')[6])
                if year != 2015:
                    continue
                InitialTitle=line.strip().split('\t')[4]
                if isEnglish(InitialTitle)==False:
                    continue
                

                Title='# ' + InitialTitle + ' .'

                ActualTitles.append(InitialTitle.split())

                #Conference[area][conf]+=(['#']+InitialTitle.split()+['.'])
                Conference[area][conf].append(Title.split())

                AllWords+=Title.split()

                if len(InitialTitle.split())+1 > maxSeqlength:
                    maxSeqlength=len(Title.split())+1

                paperCount+=1







with tf.Session() as sess:   
    saver = tf.train.import_meta_graph('../Models/model.ckpt.meta')
    saver.restore(sess,tf.train.latest_checkpoint('../Models/'))


    vocab_size=sess.run('vocab_size:0')
    maxSeqlength=sess.run('maxSeqlength:0')
    n_hidden=sess.run('n_hidden:0')





    '''x = tf.placeholder("float", [None, maxSeqlength, 1],name='x')
    y = tf.placeholder("float", [None, vocab_size],name='y')
    seqlen = tf.placeholder(tf.int32, [None],name='seqlen')'''



    graph=tf.get_default_graph()

    x = graph.get_tensor_by_name("x:0")
    y = graph.get_tensor_by_name("y:0")
    seqlen = graph.get_tensor_by_name("seqlen:0")

    '''with open('variables.txt','w')as outputfile:
        for i in graph.get_operations():
            outputfile.write(i.name+'\n')'''

            
    weights=graph.get_tensor_by_name("weights:0")
    #print(weights.eval()[0])      
    biases=graph.get_tensor_by_name("biases:0")
    #print('#########################################')
    pred = graph.get_tensor_by_name("pred:0")
    #print('$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$')
    cost=graph.get_tensor_by_name("cost:0")
    correct_pred=graph.get_tensor_by_name("correct_pred:0")
    accuracy=graph.get_tensor_by_name("accuracy:0")

    with open('../Models/dictionary.json', 'r') as fp:
        dictionary=json.load(fp)
    with open('../Models/reverse_dictionary.json', 'r') as fp:
        reverse_dictionary=json.load(fp)

    
    embeddingsDict,embeddingsArray=loadGloVe(embedFile,dictionary,reverse_dictionary)
    embedding_dim = len(embeddingsArray[0])


    '''def dynamicRNN(x, seqlen, weights, biases):
        x = tf.unstack(x, maxSeqlength, 1)
        lstm_cell = tf.contrib.rnn.BasicLSTMCell(n_hidden)
        outputs, states = tf.contrib.rnn.static_rnn(lstm_cell, x, dtype=tf.float32,
                                    sequence_length=seqlen)
        outputs = tf.stack(outputs)
        outputs = tf.transpose(outputs, [1, 0, 2])

        batch_size = tf.shape(outputs)[0]
        index = tf.range(0, batch_size) * maxSeqlength + (seqlen - 1)
        outputs = tf.gather(tf.reshape(outputs, [-1, n_hidden]), index)

        return tf.add(tf.matmul(outputs, weights),biases)'''



    #testing_data = ToySequenceData(Conference[area][conf], maxSeqlength)         
    #init = tf.global_variables_initializer()


    
    

    #sess.run(init)
    #print(weights.eval()[0])
    #sys.exit()

    predictedNumberofPaper=len(ActualTitles)

    RefereneTitles=[]
    #for i in range(len(ActualTitles)):
    for i in range(len(ActualTitles)):
        RefereneTitles.append(ActualTitles)

    BLEUscores=[]

    print('Predicted Number of Papers: '+str(predictedNumberofPaper))
    for iteration in range(10):
        PredictedTitles=[]
        IndividualBLEU={}
        PairWiseBLEU={}
        MostSimilarTitle={}
        
        with open('output.txt','w') as outputfile:
            for i in range(predictedNumberofPaper):
                while(True):
                    sentence=[]
                    sentence.append([])
                    sentence[0].append(embeddingsDict['#'])
                    sentence[0]+=[[0.0 for m in range(embedding_dim)] for j in range(maxSeqlength - 1)]
                    s='# '

                    temparatue=0.10
                    
                    for j in range(1,maxSeqlength):
                        #print('shape:' +str(np.shape(sentence)))
                        onehot_pred = sess.run(pred, feed_dict={x: sentence, seqlen: [j]})

                        Minimum=min(onehot_pred[0])
                        mysum=0.0
                        if Minimum<0:
                            for k in range(len(onehot_pred[0])):
                                onehot_pred[0][k]+=-Minimum+1
                                mysum+=onehot_pred[0][k]
                        k=0
                        multinomial=[]
                        for k in range(len(onehot_pred[0])):
                            multinomial.append(onehot_pred[0][k]/mysum)
                            k+=1

                        for number in multinomial:
                            if number <=0:
                                print(number)

                        onehot_pred_index=sample(multinomial,temparatue)
                        temparatue=max(0.02,temparatue/2)


                        '''cdf = [multinomial[0]]
                        for k in range(1, len(multinomial)):
                            cdf.append(cdf[-1] + multinomial[k])
                        onehot_pred_index = bisect(cdf,random())'''

                        
                        #onehot_pred_index = (int(tf.argmax(onehot_pred, 1).eval()))
                        
                        sentence[0][j]=embeddingsDict[reverse_dictionary[str(onehot_pred_index)]]
                        #print(reverse_dictionary[onehot_pred_index])
                        s+=reverse_dictionary[str(onehot_pred_index)]+' '
                        if reverse_dictionary[str(onehot_pred_index)]=='.':
                            break
                    if j>=10:
                        break
                outputfile.write(str(i+1)+' '+ str(len(s.split()))+' '+s+'\n\n')
                #print('iteration: '+str(iteration)+'\tgenerated sentence: '+str(i+1))
                print(str(i+1)+': '+ s)
                PredictedTitles.append(s.split())


                chencherry = SmoothingFunction()
                #IndividualBLEU[i]=rougescore.rouge_l(s.split(),RefereneTitles[0],0.5)
                PairWiseBLEU[i]={}
                for j in range(len(RefereneTitles[0])):                        
                    #PairWiseBLEU[i][j]=bleu_score.sentence_bleu([RefereneTitles[0][j]],s.split()[1:-1],weights=(0.0, 0.0, 0.5, 0.5),smoothing_function=chencherry.method2)
                    #PairWiseBLEU[i][j]=float(bleu_score.modified_precision([RefereneTitles[0][j]],s.split()[1:-1],3))
                    PairWiseBLEU[i][j]=rougescore.rouge_3(remove_stopwords(s.split()[1:-1]),remove_stopwords([RefereneTitles[0][j]]),0.50)
                MostSimilarTitleNo=sorted(PairWiseBLEU[i],key=PairWiseBLEU[i].get, reverse=True)[0]
                IndividualBLEU[i]=PairWiseBLEU[i][MostSimilarTitleNo]
                MostSimilarTitle[i]=RefereneTitles[0][MostSimilarTitleNo]


        for titleNo in sorted(IndividualBLEU,key=IndividualBLEU.get, reverse=True)[:10]:
            print(str(IndividualBLEU[titleNo])+'\n'+' '.join(PredictedTitles[titleNo][1:-1])+'\n\n'+' '.join(MostSimilarTitle[titleNo])+'\n---------------------------------\n\n')

        mybleu=sum(IndividualBLEU.values())/len(IndividualBLEU)
        BLEUscores.append(mybleu)
        print('\n\niteration: '+str(iteration)+'\tBlue Score: '+str(mybleu))
        break
                
        
                
print(numpy.average(BLEUscores),numpy.std(BLEUscores))

    

    






