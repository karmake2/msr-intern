import sys
import shutil
import os
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'
from os import listdir
from os.path import isfile, join
import numpy
import NDCG
import RBO
from bisect import bisect
from random import random
from nltk.translate import bleu_score
from nltk.translate.bleu_score import SmoothingFunction
import numpy as np
import tensorflow as tf
from tensorflow.contrib import rnn
import random
import collections
import time
import codecs


def isEnglish(s):
    try:
        codecs.encode(s,'ascii')
    except:
        return False
    else:
        return True

    

DataDirectory='/home/t-shsant/Data/OrganisedData/'
OutputDirectory='/home/t-shsant/Data/CleanData/'

Stopwords=[]

with open('stop-word-list.txt','r')as inputfile:
    for line in inputfile:
        Stopwords.append(line.strip().lower())


 
start_time = time.time()
def elapsed(sec):
    if sec<60:
        return str(sec) + " sec"
    elif sec<(60*60):
        return str(sec/60) + " min"
    else:
        return str(sec/(60*60)) + " hr"



def prepare_data(content):
    content = np.array(content)
    content = np.reshape(content, [-1, ])
    return content


def build_dataset(words):
    count = collections.Counter(words).most_common()
    dictionary = dict()
    for word, _ in count:
        dictionary[word] = len(dictionary)
    reverse_dictionary = dict(zip(dictionary.values(), dictionary.keys()))
    return dictionary, reverse_dictionary


#areas=next(os.walk('/home/t-shsant/Data/OrganisedData/'))[1]
areas=['ML']




# Parameters
learning_rate = 0.0001
training_iters = 500000
display_step = 100
n_input = 3
Total_epoch=500


# number of units in RNN cell
n_hidden = 512



def RNN(x, weights, biases):
    x = tf.reshape(x, [-1, n_input])
    x = tf.split(x,n_input,1)

    rnn_cell = rnn.MultiRNNCell([rnn.BasicLSTMCell(n_hidden),rnn.BasicLSTMCell(n_hidden)])
    #rnn_cell = rnn.BasicLSTMCell(n_hidden)
    outputs, states = rnn.static_rnn(rnn_cell, x, dtype=tf.float32)

    return tf.matmul(outputs[-1], weights['out']) + biases['out']





Conference={}
for area in sorted(areas):
    confs=['kdd']
    Conference[area]={}
    
    for confname in confs:
        ActualTitles=[]
        conf=confname.replace('.txt','')
        Conference[area][conf]=[]

        paperCount=0
        
        with open(DataDirectory+area+'/'+conf+'.txt','r') as inputfile:
            for line in inputfile:
                year=int(line.split('\t')[6])
                if year<2016:
                    continue
                InitialTitle=line.strip().split('\t')[4]
                if isEnglish(InitialTitle)==False:
                    continue
                ActualTitles.append(InitialTitle)

                Title=''
                for word in InitialTitle.split():
                    if word not in Stopwords:
                        Title+=word+' '

                Conference[area][conf]+=(['#']+InitialTitle.split()+['.'])
                paperCount+=1

                
                if paperCount>30:
                    break
                
        print(len(Conference[area][conf]))
                
        
        training_data=prepare_data(Conference[area][conf])
        print("Loaded training data...")

        dictionary, reverse_dictionary = build_dataset(training_data)
        vocab_size = len(dictionary)




        # tf Graph input
        x = tf.placeholder("float", [None, n_input, 1])
        y = tf.placeholder("float", [None, vocab_size])



        # RNN output node weights and biases
        weights = {
            'out': tf.Variable(tf.random_normal([n_hidden, vocab_size]))
        }
        biases = {
            'out': tf.Variable(tf.random_normal([vocab_size]))
        }


        pred = RNN(x, weights, biases)

        # Loss and optimizer
        cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=pred, labels=y))
        optimizer = tf.train.RMSPropOptimizer(learning_rate=learning_rate).minimize(cost)

        # Model evaluation
        correct_pred = tf.equal(tf.argmax(pred,1), tf.argmax(y,1))
        accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))

        # Initializing the variables
        init = tf.global_variables_initializer()


        
        with tf.Session() as session:
            session.run(init)
            step = 1
            #offset = random.randint(0,n_input+1)
            offset=0
            end_offset = n_input + 1
            acc_total = 0
            loss_total = 0
            epoch=0

            #writer.add_graph(session.graph)

            while epoch <Total_epoch:
                symbols_in_keys = [ [dictionary[ str(training_data[i])]] for i in range(offset, offset+n_input) ]
                symbols_in_keys = np.reshape(np.array(symbols_in_keys), [-1, n_input, 1])

                symbols_out_onehot = np.zeros([vocab_size], dtype=float)
                symbols_out_onehot[dictionary[str(training_data[offset+n_input])]] = 1.0
                symbols_out_onehot = np.reshape(symbols_out_onehot,[1,-1])

                _, acc, loss, onehot_pred = session.run([optimizer, accuracy, cost, pred], \
                                                        feed_dict={x: symbols_in_keys, y: symbols_out_onehot})

                loss_total += loss
                acc_total += acc

                if step % display_step==0:
                    symbols_in = [training_data[i] for i in range(offset, offset + n_input)]
                    symbols_out = training_data[offset + n_input]
                    symbols_out_pred = reverse_dictionary[int(tf.argmax(onehot_pred, 1).eval())]
                    #print("%s - [%s] vs [%s]" % (symbols_in,symbols_out,symbols_out_pred))

                if offset+end_offset >=(len(training_data)):
                    print("epoch= " + str(epoch+1) + ", Average Loss= " + \
                          "{:.6f}".format(loss_total/offset) + ", Average Accuracy= " + \
                          "{:.2f}%".format(100*acc_total/offset))
                    
                    offset=-1
                    epoch+=1
                    acc_total = 0
                    loss_total = 0
                offset += 1
                step+=1


            print("Optimization Finished!")
            print("Elapsed time: ", elapsed(time.time() - start_time))
            print("Run on command line.")
            #print("\ttensorboard --logdir=%s" % (logs_path))
            print("Point your web browser to: http://localhost:6006/")
            while True:
                prompt = "%s words: " % n_input
                sentence = input(prompt)
                sentence = sentence.strip()
                words = sentence.split(' ')
                if len(words) != n_input:
                    continue
                try:
                    symbols_in_keys = [dictionary[str(words[i])] for i in range(len(words))]
                    for i in range(30):
                        keys = np.reshape(np.array(symbols_in_keys), [-1, n_input, 1])
                        onehot_pred = session.run(pred, feed_dict={x: keys})
                        onehot_pred_index = int(tf.argmax(onehot_pred, 1).eval())
                        sentence = "%s %s" % (sentence,reverse_dictionary[onehot_pred_index])
                        symbols_in_keys = symbols_in_keys[1:]
                        symbols_in_keys.append(onehot_pred_index)
                    print(sentence)
                except:
                    print("Word not in dictionary")
        
                


                
'''
            RefereneTitles=[]
            for i in range(len(ActualTitles)):
                RefereneTitles.append(ActualTitles)


            NDCGscores=[]
            RBOscores=[]
            BLEUscores=[]

            
            for iteration in range(10):
                PredictedTitles=[]

                #print('generating paper: '+ str(itoYear[TrainingEndTime+t]))

                GeneratedBigramDistribution={}

                for i in range(predictedNumberofPaper):
                    generatedTitle=[]
                    for j in range(int(predictedAverageLength/2)):
                        selectedBigram=itobigram[bisect(cdf,random())]
                        words=selectedBigram.split()
                        for word in words:
                            generatedTitle.append(word)

                        if selectedBigram not in GeneratedBigramDistribution:
                            GeneratedBigramDistribution[selectedBigram]=1
                        else:
                            GeneratedBigramDistribution[selectedBigram]+=1
                    #print(generatedTitle)
                    PredictedTitles.append(generatedTitle)


                
                    
                chencherry = SmoothingFunction()
                x=(bleu_score.corpus_bleu(RefereneTitles,PredictedTitles,smoothing_function=chencherry.method3))
                BLEUscores.append(x)
            
            print(itoYear[TrainingEndTime+t],numpy.average(BLEUscores),numpy.std(BLEUscores))
        print('\n\n\n')'''



















        
        
