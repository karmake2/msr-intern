import math

def computeNDCGatK(RankedList,IdealList,K):
    DCG=0.0
    for i in range(0,K):
        if RankedList[i] in IdealList[0:K]:
            relevance=K-IdealList.index(RankedList[i])
        else:
            relevance=0.0
        DCG+=relevance/math.log(i+2,2)

    IDCG=0.0
    for i in range(0,K):
        IDCG+=(K-i)/math.log(i+2,2)

    #print(DCG,IDCG)
    return DCG/IDCG
        
    
        
    
