import sys
import shutil
import os

DataDirectory='/home/t-shsant/Data/MasterData/'
#DataDirectory='D:/Santu/Data/'
OutputDirectory='/home/t-shsant/Data/OrganisedData/'


Conference={}
ConferenceAreaMapping={}
ConferenceList=[]


try:
    shutil.rmtree(OutputDirectory)
except:
    print('No Directory')

os.makedirs(OutputDirectory)



with open(DataDirectory+'Conferences.txt','r') as inputfile:
    for line in inputfile:
        area=line.split(':')[0]
        Conference[area]={}

        confList=line.strip().split(':')[1].lower().split(',')
        for conf in confList:
            Conference[area]={}
            ConferenceList.append(conf)
            ConferenceAreaMapping[conf]=area




with open(DataDirectory+'AcademicData.tsv') as inputfile:
    for line in inputfile:
        year=int(line.split('\t')[6])
        conf=line.split('\t')[11].strip()
        if conf not in ConferenceList:
            continue
        else:
            if year not in Conference[ConferenceAreaMapping[conf]]:
                Conference[ConferenceAreaMapping[conf]][year]=[]
            Conference[ConferenceAreaMapping[conf]][year].append(line)



        
for area in Conference:
    #print('\n\n\n\n##########   '+area+'    ############# \n\n\n')
    with open(OutputDirectory+'/'+area+'.txt','w') as outputfile:
        deleteList=[]
        for year in sorted(Conference[area]):
            if len(Conference[area][year])<20 or year==2017:
                #print(conf+ ': ignoring year '+str(year)+' count: '+str(len(Conference[area][conf][year])))
                deleteList.append(year)
                continue
            for paper in Conference[area][year]:
                outputfile.write(paper)
        for year in deleteList:
            del Conference[area][year]
        

for area in Conference:
    papercounts=0
    yearcounts=len(Conference[area])
    
    for year in Conference[area]:
        papercounts+=len(Conference[area][year])
        print(year,len(Conference[area][year]))
    print(area,yearcounts,"{0:.2f}".format(papercounts/float(yearcounts)))















        
        
