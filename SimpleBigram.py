import sys
import shutil
import os
from os import listdir
from os.path import isfile, join
import numpy
from statsmodels.tsa.api import VAR, DynamicVAR
import NDCG
import RBO
from bisect import bisect
from random import random
from nltk.translate import bleu_score
from nltk.translate.bleu_score import SmoothingFunction
import codecs
import rougescore



def isEnglish(s):
    try:
        codecs.encode(s,'ascii')
    except:
        return False
    else:
        return True


    

DataDirectory='/home/t-shsant/Data/OrganisedData/'
OutputDirectory='/home/t-shsant/Data/CleanData/'

TestYear=2015

#areas=next(os.walk('/home/t-shsant/Data/OrganisedData/'))[1]
areas=['ML']
Stopwords=[]


with open('stop-word-list.txt','r')as inputfile:
    for line in inputfile:
        Stopwords.append(line.strip().lower())
        


def remove_stopwords(sentence):
    cleanSentence=[]
    for word in sentence:
        if word not in Stopwords:
            cleanSentence.append(word)
    return cleanSentence




for area in sorted(areas):
    #confs = [f for f in listdir(DataDirectory+area) if isfile(join(DataDirectory+area, f))]
    confs=['kdd']
    
    for confname in confs:
        TotalPapers=0
        BigramCounts={}
        NumberofPapers={}
        ActualTitles={}
        conf=confname.replace('.txt','')
        MaxTitleLen=0
        UnigramMapping={}
        UnigramReverseMapping={}
        UnigramCounter=0

        
        with open(DataDirectory+area+'/'+conf+'.txt','r') as inputfile:
            for line in inputfile:
                TotalPapers+=1
                year=int(line.split('\t')[6])

                if year > TestYear:
                    continue

                InitialTitle=line.strip().split('\t')[4]
                if isEnglish(InitialTitle)==False:
                    continue
                

                Title=''
                for word in InitialTitle.split():
                    if word not in Stopwords:
                        Title+=word+' '

                Title='# ' + InitialTitle + ' .'

                TitleWords=Title.split()
                BigramSet=[]
                for i in range(len(TitleWords)-1):
                    BigramSet.append(TitleWords[i]+' '+TitleWords[i+1])
                BigramSet=list(set(BigramSet))

                
                if year <= TestYear:
                    for bigram in BigramSet:
                        if bigram not in BigramCounts:
                            BigramCounts[bigram]=1
                        else:
                            BigramCounts[bigram]+=1

                if year not in NumberofPapers:
                    NumberofPapers[year]=1
                else:
                    NumberofPapers[year]+=1

                


                if year not in ActualTitles:
                    ActualTitles[year]=[]
                ActualTitles[year].append(InitialTitle.split())

                if len(TitleWords) > MaxTitleLen:
                    MaxTitleLen=len(TitleWords)


                for word in TitleWords:
                    if word not in UnigramMapping:
                        UnigramMapping[word]=UnigramCounter
                        UnigramReverseMapping[UnigramCounter]=word
                        UnigramCounter+=1



        predictions={}
        itobigram={}

        mysum=0.0
        for bigram in BigramCounts:
            mysum+=BigramCounts[bigram]
        
        i=0
        multinomial=[]
        for bigram in sorted(BigramCounts):
            itobigram[i]=bigram
            multinomial.append(BigramCounts[bigram]/mysum)
            i+=1

        i=0
        multinomial_conditional={}
        for bigram in sorted(BigramCounts):
            first_word=bigram.split()[0]
            second_word=bigram.split()[1]

            if first_word not in multinomial_conditional:
                multinomial_conditional[first_word]=[]
                for j in range(len(UnigramReverseMapping)):
                    multinomial_conditional[first_word].append(0.0)
            multinomial_conditional[first_word][UnigramMapping[second_word]]=(BigramCounts[bigram])
            i+=1


        for first_word in multinomial_conditional:
            mysum=sum(multinomial_conditional[first_word])
            for i in range(len(multinomial_conditional[first_word])):
                multinomial_conditional[first_word][i]/=mysum

        predictedNumberofPaper=int(NumberofPapers[TestYear])
        
        cdf = [multinomial[0]]
        for i in range(1, len(multinomial)):
            cdf.append(cdf[-1] + multinomial[i])

        cdf_conditional={}
        for first_word in multinomial_conditional:
            cdf_conditional[first_word] = [multinomial_conditional[first_word][0]]
            for i in range(1, len(multinomial_conditional[first_word])):
                cdf_conditional[first_word].append(cdf_conditional[first_word][-1] + multinomial_conditional[first_word][i])
    

        '''for index in sorted(range(len(multinomial)),key=lambda x:multinomial[x],reverse=True)[:20]:
            print(itobigram[index],output[0][index])
        print('\n\n')'''

        RefereneTitles=[]
        RefereneTitles+=ActualTitles[TestYear]
            



        BLEUscores=[]

        
        for iteration in range(10):
            PredictedTitles=[]
            IndividualBLEU={}
            PairWiseBLEU={}
            MostSimilarTitle={}

            #print('generating paper: '+ str(itoYear[TrainingEndTime+t]))

            GeneratedBigramDistribution={}

            for i in range(predictedNumberofPaper):
                while(True):
                    generatedTitle=[]
                    generatedTitle.append('#')
                    for j in range(1,MaxTitleLen):
                        if generatedTitle[-1] in cdf_conditional:
                            selectedUnigram=UnigramReverseMapping[bisect(cdf_conditional[generatedTitle[-1]],random())]
                        else:
                            selectedUnigram=itobigram[bisect(cdf,random())].split()[0]
                        generatedTitle.append(selectedUnigram)
                        if selectedUnigram=='.':
                            break
                    if j>=10:
                        break
                PredictedTitles.append(generatedTitle)
                chencherry = SmoothingFunction()

                PairWiseBLEU[i]={}
                for j in range(len(RefereneTitles)):
                    PairWiseBLEU[i][j]=rougescore.rouge_3(remove_stopwords(generatedTitle[1:-1]),remove_stopwords([RefereneTitles[j]]),0.50)
                MostSimilarTitleNo=sorted(PairWiseBLEU[i],key=PairWiseBLEU[i].get, reverse=True)[0]
                IndividualBLEU[i]=PairWiseBLEU[i][MostSimilarTitleNo]
                MostSimilarTitle[i]=RefereneTitles[MostSimilarTitleNo]
                

            for titleNo in sorted(IndividualBLEU,key=IndividualBLEU.get, reverse=True)[:10]:
                print(str(IndividualBLEU[titleNo])+'\n'+' '.join(PredictedTitles[titleNo][1:-1])+'\n\n'+' '.join(MostSimilarTitle[titleNo])+'\n---------------------------------\n\n')
                
            mybleu=sum(IndividualBLEU.values())/len(IndividualBLEU)
            BLEUscores.append(mybleu)
            print('\n\niteration: '+str(iteration)+'\tBlue Score: '+str(mybleu))
            break
                      
            '''chencherry = SmoothingFunction()
            x=(bleu_score.corpus_bleu(RefereneTitles,PredictedTitles,weights=(0.0, 0.0, 0.0, 1.00),smoothing_function=chencherry.method5))
            BLEUscores.append(x)'''

            

        
            '''print('Actual-----------------')
            for bigram in sorted(BigramCounts[itoYear[TrainingEndTime+t]],key=BigramCounts[itoYear[TrainingEndTime+t]].get,reverse=True)[:10]:
                print(bigram,BigramCounts[itoYear[TrainingEndTime+t]][bigram])

            print('Predicted-----------------')
            for bigram in sorted(GeneratedBigramDistribution,key=GeneratedBigramDistribution.get,reverse=True)[:20]:
                print(bigram,GeneratedBigramDistribution[bigram])
            print('\n\n')

            RankedList=[]
            for mytuple in sorted(GeneratedBigramDistribution.items(),key=lambda x: (x[1],x[0]),reverse=True):
                RankedList.append(mytuple[0])

            for index in sorted(range(len(multinomial)),key=lambda x:multinomial[x],reverse=True):
                RankedList.append(itobigram[index])


            IdealList=[]
            for mytuple in sorted(BigramCounts[itoYear[TrainingEndTime+t]].items(),key=lambda x: (x[1],x[0]),reverse=True):
                IdealList.append(mytuple[0])

            #print(itoYear[TrainingEndTime+t],NDCG.computeNDCGatK(RankedList,IdealList,10), RBO.computeRBOatK(RankedList,IdealList,10))
            #NDCGscores.append(NDCG.computeNDCGatK(RankedList,IdealList,20))
            #RBOscores.append(RBO.computeRBOatK(RankedList,IdealList,20))
            #print(itoYear[TrainingEndTime+t],iteration,x)'''
        

        print(TestYear,numpy.average(BLEUscores),numpy.std(BLEUscores))
    print('\n\n\n')


















        
        
