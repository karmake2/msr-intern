import sys
import shutil
import os
from os import listdir
from os.path import isfile, join
import numpy
from statsmodels.tsa.api import VAR, DynamicVAR
import NDCG
import RBO
from bisect import bisect
from random import random
from nltk.translate import bleu_score
from nltk.translate.bleu_score import SmoothingFunction
from pandas import datetime
from statsmodels.tsa.arima_model import ARIMA


ARorder=3
DataDirectory='/home/t-shsant/Data/OrganisedData/'
OutputDirectory='/home/t-shsant/Data/CleanData/'

#areas=next(os.walk('/home/t-shsant/Data/OrganisedData/'))[1]
areas=['ML']
Stopwords=[]


with open('stop-word-list.txt','r')as inputfile:
    for line in inputfile:
        Stopwords.append(line.strip().lower())
        
    

for area in sorted(areas):
    IDFall={}
    Conference={}
    Allyears=set([])
    AllBigrams=[]
    TotalPapers=0
    BigramCounts={}
    Conference[area]={}
    NumberofPapers={}
    AveragePaperLength={}
    ActualTitles={}
    
    #confs = [f for f in listdir(DataDirectory+area) if isfile(join(DataDirectory+area, f))]
    confs=['kdd']
    
    for confname in confs:
        conf=confname.replace('.txt','')
        Conference[area][conf]={}
        with open(DataDirectory+area+'/'+conf+'.txt','r') as inputfile:
            for line in inputfile:
                TotalPapers+=1
                year=int(line.split('\t')[6])
                if year not in Conference[area][conf]:
                    Conference[area][conf][year]=[]
                Conference[area][conf][year].append(line.strip())
                InitialTitle=line.strip().split('\t')[4]

                Title=''
                for word in InitialTitle.split():
                    if word not in Stopwords:
                        Title+=word+' '

                TitleWords=Title.split()
                BigramSet=[]
                for i in range(len(TitleWords)-1):
                    BigramSet.append(TitleWords[i]+' '+TitleWords[i+1])
                BigramSet=list(set(BigramSet))

                Allyears.add(year)

                for bigram in BigramSet:
                    if bigram not in IDFall:
                        IDFall[bigram]=1
                        AllBigrams.append(bigram)
                    else:
                        IDFall[bigram]+=1

                if year not in BigramCounts:
                    BigramCounts[year]={}

                for bigram in BigramSet:
                    if bigram not in BigramCounts[year]:
                        BigramCounts[year][bigram]=1
                    else:
                        BigramCounts[year][bigram]+=1

                if year not in NumberofPapers:
                    NumberofPapers[year]=1
                else:
                    NumberofPapers[year]+=1

                if year not in AveragePaperLength:
                    AveragePaperLength[year]=len(TitleWords)
                else:
                    AveragePaperLength[year]+=len(TitleWords)

                if year not in ActualTitles:
                    ActualTitles[year]=[]
                ActualTitles[year].append(TitleWords)

                
    #print(len(IDFall))

    IDF={}
    for bigram in sorted(IDFall,key=IDFall.get,reverse=True)[:5000]:
        IDF[bigram]=IDFall[bigram]
        
        
    for year in AveragePaperLength:
        AveragePaperLength[year]=AveragePaperLength[year]/NumberofPapers[year]

    
    for year in Allyears:
        for bigram in IDF:
            if bigram not in BigramCounts[year]:
                BigramCounts[year][bigram]=0


    '''
    for year in BigramCounts:
        for bigram in BigramCounts[year]:
            try:
                BigramCounts[year][bigram]=BigramCounts[year][bigram]*TotalPapers/IDF[bigram]
            except:
                BigramCounts[year][bigram]=0.0
    '''

    PaperCountSeries=[]
    TimeSeries=[]
    itoYear={}
    i=0
    for year in sorted(BigramCounts):
        itoYear[i]=year
        TimeSeries.append([])
        Denominator=0
        for bigram in sorted(IDF):
            Denominator+=BigramCounts[year][bigram]
        Denominator+=len(IDF)
        for bigram in sorted(IDF):
            #TimeSeries[i].append(float(BigramCounts[year][bigram]+1)/Denominator)
            TimeSeries[i].append(float(BigramCounts[year][bigram]))
        i+=1


    
    TimeSeries=numpy.array(TimeSeries)
    TimeSeries=numpy.transpose(TimeSeries)

    TrainingEndTime=int(len(TimeSeries[0])*0.8)
    train=list(TimeSeries[:,0:TrainingEndTime])
    test=list(TimeSeries[:,TrainingEndTime:len(TimeSeries)])




    model=[]
    model_fit=[]

    for i in range(len(train)):
        model.append(ARIMA(train[i], order=(ARorder,0,0)))
        try:
            model_fit.append(model[i].fit(disp=0))
        except:
            model_fit.append(None)
        if i%100==0:
            print('training for '+str(i))
        
        


    
    itobigram={}
    
    for t in range(len(test)):
        output=[]
        for i in range(len(train)):
            if model_fit[i] is not None:
                output.append(model_fit[i].forecast())
            else:
                output.append(0.0)
        predictions={}

        mysum=0.0
        for i in range(len(output)):
            mysum+=output[i]
        
        i=0
        multinomial=[]
        for bigram in sorted(IDF):
            itobigram[i]=bigram
            multinomial.append(output[i]/mysum)
            i+=1

        predictedNumberofPaper=int(NumberofPapers[itoYear[TrainingEndTime+t]])
        predictedAverageLength=int(AveragePaperLength[itoYear[TrainingEndTime+t-1]])
        
        cdf = [multinomial[0]]
        for i in range(1, len(multinomial)):
            cdf.append(cdf[-1] + multinomial[i])


        '''for index in sorted(range(len(multinomial)),key=lambda x:multinomial[x],reverse=True)[:20]:
            print(itobigram[index],output[0][index])
        print('\n\n')'''

        RefereneTitles=[]
        for i in range(len(ActualTitles[itoYear[TrainingEndTime+t]])):
            RefereneTitles.append(ActualTitles[itoYear[TrainingEndTime+t]])


        NDCGscores=[]
        RBOscores=[]
        BLEUscores=[]

        
        for iteration in range(10):
            PredictedTitles=[]

            #print('generating paper: '+ str(itoYear[TrainingEndTime+t]))

            GeneratedBigramDistribution={}

            for i in range(predictedNumberofPaper):
                generatedTitle=[]
                for j in range(int(predictedAverageLength/2)):
                    selectedBigram=itobigram[bisect(cdf,random())]
                    words=selectedBigram.split()
                    for word in words:
                        generatedTitle.append(word)

                    if selectedBigram not in GeneratedBigramDistribution:
                        GeneratedBigramDistribution[selectedBigram]=1
                    else:
                        GeneratedBigramDistribution[selectedBigram]+=1
                #print(generatedTitle)
                PredictedTitles.append(generatedTitle)


            
                
            chencherry = SmoothingFunction()
            x=(bleu_score.corpus_bleu(RefereneTitles,PredictedTitles,smoothing_function=chencherry.method3))
            BLEUscores.append(x)
            
            '''print('Actual-----------------')
            for bigram in sorted(BigramCounts[itoYear[TrainingEndTime+t]],key=BigramCounts[itoYear[TrainingEndTime+t]].get,reverse=True)[:10]:
                print(bigram,BigramCounts[itoYear[TrainingEndTime+t]][bigram])

            print('Predicted-----------------')
            for bigram in sorted(GeneratedBigramDistribution,key=GeneratedBigramDistribution.get,reverse=True)[:20]:
                print(bigram,GeneratedBigramDistribution[bigram])
            print('\n\n')'''

            RankedList=[]
            '''for mytuple in sorted(GeneratedBigramDistribution.items(),key=lambda x: (x[1],x[0]),reverse=True):
                RankedList.append(mytuple[0])'''

            for index in sorted(range(len(multinomial)),key=lambda x:multinomial[x],reverse=True):
                RankedList.append(itobigram[index])


            IdealList=[]
            for mytuple in sorted(BigramCounts[itoYear[TrainingEndTime+t]].items(),key=lambda x: (x[1],x[0]),reverse=True):
                IdealList.append(mytuple[0])

            #print(itoYear[TrainingEndTime+t],NDCG.computeNDCGatK(RankedList,IdealList,10), RBO.computeRBOatK(RankedList,IdealList,10))
            #NDCGscores.append(NDCG.computeNDCGatK(RankedList,IdealList,20))
            #RBOscores.append(RBO.computeRBOatK(RankedList,IdealList,20))
            print(itoYear[TrainingEndTime+t],iteration,x)
        

        print('ashlam')
        for i in range(len(train)):
            train[i].append(test[i][t])
        model=[]
        model_fit=[]

        for i in range(len(train[0])):
            model.append(ARIMA(train[i], order=(ARorder,0,0)))
            model_fit.append(model[i].fit(ARorder))
        
        print('\n\n\n')
        print(itoYear[TrainingEndTime+t],numpy.average(BLEUscores),numpy.std(BLEUscores))
        print('\n\n\n')



















        
        
