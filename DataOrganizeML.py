import sys
import shutil
import os
import random

DataDirectory='/home/t-shsant/Data/MasterData/'
#DataDirectory='D:/Santu/Data/'
OutputDirectory='/home/t-shsant/Data/OrganisedData/'


Papers={}
area='ML'

SelectedPapers={}

try:
    shutil.rmtree(OutputDirectory)
except:
    print('No Directory')

os.makedirs(OutputDirectory)



with open(DataDirectory+'AcademicData.tsv') as inputfile:
    for line in inputfile:
        year=int(line.split('\t')[6])

        if year<1980:
            continue
        if year not in Papers:
            Papers[year]=[]
        Papers[year].append(line)



with open(OutputDirectory+area+'.txt','w') as outputfile:
    for year in sorted(Papers):
        if len(Papers[year])<20 or year==2017:
            continue

        if year not in SelectedPapers:
            SelectedPapers[year]=[]
            
        random.shuffle(Papers[year])
        for paper in Papers[year][:2000]:
            outputfile.write(paper)
            SelectedPapers[year].append(paper)
        

papercounts=0
yearcounts=len(SelectedPapers)

for year in sorted(SelectedPapers):
    papercounts+=len(SelectedPapers[year])
    print(year,len(SelectedPapers[year]))
print(yearcounts,"{0:.2f}".format(papercounts/float(yearcounts)))















        
        
