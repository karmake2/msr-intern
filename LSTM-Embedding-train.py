import sys
import shutil
import os
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'
from os import listdir
from os.path import isfile, join
import numpy
import NDCG
import RBO
from bisect import bisect
from random import random
from nltk.translate import bleu_score
from nltk.translate.bleu_score import SmoothingFunction
import numpy as np
import tensorflow as tf
from tensorflow.contrib import rnn
import random
from random import random
import collections
import time
import codecs
import json


def isEnglish(s):
    try:
        codecs.encode(s,'ascii')
    except:
        return False
    else:
        return True
    

    

DataDirectory='/home/t-shsant/Data/OrganisedData/'
embedFile = '../Data/glove.6B.100d.txt'

Stopwords=[]

with open('stop-word-list.txt','r')as inputfile:
    for line in inputfile:
        Stopwords.append(line.strip().lower())
        


 
start_time = time.time()
def elapsed(sec):
    if sec<60:
        return str(sec) + " sec"
    elif sec<(60*60):
        return str(sec/60) + " min"
    else:
        return str(sec/(60*60)) + " hr"
    



def prepare_data(content):
    content = np.array(content)
    content = np.reshape(content, [-1, ])
    return content






def build_dataset(words):
    count = collections.Counter(words).most_common()
    dictionary = dict()
    for word, _ in count:
        dictionary[word] = len(dictionary)+1
    dictionary['$emp$']=0
    reverse_dictionary = dict(zip(dictionary.values(), dictionary.keys()))
    return dictionary, reverse_dictionary




def loadGloVe(embedFile):
    embeddingsDict={}
    
    file = open(embedFile,'r', encoding="utf8")
    print('Loading Embeddings!')
    
    for line in file.readlines():
        if line.split()[0] in dictionary:
            row = line.strip().split(' ')
            embeddingsDict[row[0]]=np.asarray(row[1:])
            #vocab.append(row[0])
            #embd.append(row[1:])
    file.close()

    embeddingsArray=[]
    for wordid in sorted(reverse_dictionary):
        if reverse_dictionary[wordid] not in embeddingsDict:
            embeddingsDict[reverse_dictionary[wordid]]=np.asarray([0.0 for i in range(len(row[1:]))])
        embeddingsArray.append(embeddingsDict[reverse_dictionary[wordid]])
            
    print('Embeddings Loaded!')
    
    return embeddingsDict,embeddingsArray





class ToySequenceData(object):   
    def __init__(self, raw_data, embeddingsDict, embedding_dim, max_seq_len=20):
        
        self.data = []
        self.labels = []
        self.seqlen = []
                
        for i in range(len(raw_data)):
            for j in range(2,len(raw_data[i])):
                self.seqlen.append(j)
                s=[]
                for k in range(j):
                    s.append(embeddingsDict[raw_data[i][k]])
                s+= [[0.0 for j in range(embedding_dim)] for i in range(max_seq_len - j)]
                self.data.append(s)
                symbols_out_onehot = np.zeros([vocab_size], dtype=float)
                symbols_out_onehot[dictionary[raw_data[i][j]]] = 1.0
                self.labels.append(symbols_out_onehot)
            self.batch_id = 0



                                              
    def next(self, batch_size):                                                       
        if self.batch_id == len(self.data):
            self.batch_id = 0
        batch_data = (self.data[self.batch_id:min(self.batch_id +
                                                  batch_size, len(self.data))])
        batch_labels = (self.labels[self.batch_id:min(self.batch_id +
                                                  batch_size, len(self.data))])
        batch_seqlen = (self.seqlen[self.batch_id:min(self.batch_id +
                                                  batch_size, len(self.data))])
        self.batch_id = min(self.batch_id + batch_size, len(self.data))
                                              
        return batch_data, batch_labels, batch_seqlen,self.batch_id
    
                                              

    
#areas=next(os.walk('/home/t-shsant/Data/OrganisedData/'))[1]
areas=['ML']




# Parameters
learning_rate = 0.001
training_iters = 2500
batch_size = 5000
display_step = 10

# number of units in RNN cell
n_hidden = 512



def dynamicRNN(x, seqlen, weights, biases):
    x = tf.unstack(x, maxSeqlength, 1)
    lstm_cell = tf.contrib.rnn.BasicLSTMCell(n_hidden)
    outputs, states = tf.contrib.rnn.static_rnn(lstm_cell, x, dtype=tf.float32,
                                sequence_length=seqlen)
    outputs = tf.stack(outputs)
    outputs = tf.transpose(outputs, [1, 0, 2])

    batch_size = tf.shape(outputs)[0]
    index = tf.range(0, batch_size) * maxSeqlength + (seqlen - 1)
    outputs = tf.gather(tf.reshape(outputs, [-1, n_hidden]), index)

    return tf.add(tf.matmul(outputs, weights['out']),biases['out'])





Conference={}
for area in sorted(areas):
    #confs = [f for f in listdir(DataDirectory+area) if isfile(join(DataDirectory+area, f))]
    confs=['kdd']
    Conference[area]={}

    for confname in confs:
        ActualTitles=[]
        conf=confname.replace('.txt','')
        Conference[area][conf]=[]

        paperCount=0
        maxSeqlength=0

        AllWords=[]
        
        with open(DataDirectory+area+'/'+conf+'.txt','r') as inputfile:
            for line in inputfile:
                year=int(line.split('\t')[6])
                if year > 2014:
                    continue
                InitialTitle=line.strip().split('\t')[4]
                if isEnglish(InitialTitle)==False:
                    continue
                ActualTitles.append(InitialTitle)

                Title='# ' + InitialTitle + ' .'

                #Conference[area][conf]+=(['#']+InitialTitle.split()+['.'])
                Conference[area][conf].append(Title.split())

                AllWords+=Title.split()

                if len(InitialTitle.split())+1 > maxSeqlength:
                    maxSeqlength=len(Title.split())+1

                paperCount+=1
                                              

                
        print(paperCount)
        dictionary, reverse_dictionary = build_dataset(AllWords)
        vocab_size = len(dictionary)

        embeddingsDict,embeddingsArray=loadGloVe(embedFile)
        embedding_dim = len(embeddingsArray[0])

        W = tf.Variable(tf.constant(0.0, shape=[vocab_size, embedding_dim]),trainable=False, name="W")
        embedding_placeholder = tf.placeholder(tf.float32, [vocab_size, embedding_dim])
        embedding_init = W.assign(embedding_placeholder)

        training_data = ToySequenceData(Conference[area][conf], embeddingsDict,embedding_dim, maxSeqlength)

        print(len(training_data.data))

        
        # tf Graph input
        x = tf.placeholder("float", [None, maxSeqlength, embedding_dim],name='x')
        y = tf.placeholder("float", [None, vocab_size],name='y')
        seqlen = tf.placeholder(tf.int32, [None],name='seqlen')

        tf.Variable(vocab_size,name='vocab_size')
        tf.Variable(maxSeqlength,name='maxSeqlength')
        tf.Variable(n_hidden,name='n_hidden')

        with open('../Models/dictionary.json', 'w') as fp:
            json.dump(dictionary, fp)
        with open('../Models/reverse_dictionary.json', 'w') as fp:
            json.dump(reverse_dictionary, fp)
        


        # RNN output node weights and biases
        weights = {
            'out': tf.Variable(tf.random_normal([n_hidden, vocab_size]),name='weights')
        }
        biases = {
            'out': tf.Variable(tf.random_normal([vocab_size]),name='biases')
        }


        pred = tf.identity(dynamicRNN(x,seqlen,weights, biases), name='pred')

        # Loss and optimizer
        cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=pred, labels=y),name='cost')
        optimizer = tf.train.RMSPropOptimizer(learning_rate=learning_rate).minimize(cost)

        # Model evaluation
        correct_pred = tf.equal(tf.argmax(pred,1), tf.argmax(y,1),name='correct_pred')
        accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32),name='accuracy')

        # Initializing the variables
        init = tf.global_variables_initializer()


        with tf.Session() as sess:
            sess.run(init)
            saver = tf.train.Saver()
            step = 1
            # Keep training until reach max iterations
            while step < training_iters:
                batch_x, batch_y, batch_seqlen,batch_id = training_data.next(batch_size)
                # Run optimization op (backprop)
                sess.run(optimizer, feed_dict={x: batch_x, y: batch_y,
                                               seqlen: batch_seqlen})
                if step % display_step == 0:
                    # Calculate batch accuracy
                    acc = sess.run(accuracy, feed_dict={x: batch_x, y: batch_y,
                                                        seqlen: batch_seqlen})
                    # Calculate batch loss
                    loss = sess.run(cost, feed_dict={x: batch_x, y: batch_y,
                                                     seqlen: batch_seqlen})
                    print("Iter " + str(step) + ", Minibatch Loss= " + \
                          "{:.6f}".format(loss) + ", Training Accuracy= " + \
                          "{:.5f}".format(acc))
                step += 1


            print("Optimization Finished!")
            print("Elapsed time: ", elapsed(time.time() - start_time))
            print("Run on command line.")
            #print("\ttensorboard --logdir=%s" % (logs_path))
            print("Point your web browser to: http://localhost:6006/")



            #print(tf.global_variables())
            save_path = saver.save(sess, "../Models/model.ckpt")
            print("Model saved in file: %s" % save_path)

            
            with open('output.txt','w') as outputfile:
                for i in range(5):
                    sentence=[]
                    sentence.append([])
                    sentence[0].append(embeddingsDict['#'])
                    sentence[0]+=[[0.0 for m in range(embedding_dim)] for j in range(maxSeqlength - 1)]
                    s=''
                    for j in range(1,maxSeqlength):
                        #print('shape:' +str(np.shape(sentence)))
                        #print(sentence[0][0:2])
                        onehot_pred = sess.run(pred, feed_dict={x: sentence, seqlen: [j]})

                        '''Minimum=min(onehot_pred[0])
                        mysum=0.0
                        if Minimum<0:
                            for k in range(len(onehot_pred[0])):
                                onehot_pred[0][k]+=Minimum
                                mysum+=onehot_pred[0][k]
                        k=0
                        multinomial=[]
                        for k in range(len(onehot_pred[0])):
                            multinomial.append(onehot_pred[0][k]/mysum)
                            k+=1

                        cdf = [multinomial[0]]
                        for k in range(1, len(multinomial)):
                            cdf.append(cdf[-1] + multinomial[k])
                        onehot_pred_index = bisect(cdf,random())'''

                        
                        onehot_pred_index = int(tf.argmax(onehot_pred, 1).eval())
                        
                        sentence[0][j]=embeddingsDict[reverse_dictionary[onehot_pred_index]]
                        #print(reverse_dictionary[onehot_pred_index])
                        s+=reverse_dictionary[onehot_pred_index]+' '
                        if reverse_dictionary[onehot_pred_index]=='.':
                            break                    
                    outputfile.write(str(i+1)+' '+ str(len(s.split()))+' '+s+'\n\n')
                    print('generated sentence: '+str(i+1))
                    
                
        
                


                
'''
            RefereneTitles=[]
            for i in range(len(ActualTitles)):
                RefereneTitles.append(ActualTitles)


            NDCGscores=[]
            RBOscores=[]
            BLEUscores=[]

            
            for iteration in range(10):
                PredictedTitles=[]

                #print('generating paper: '+ str(itoYear[TrainingEndTime+t]))

                GeneratedBigramDistribution={}

                for i in range(predictedNumberofPaper):
                    generatedTitle=[]
                    for j in range(int(predictedAverageLength/2)):
                        selectedBigram=itobigram[bisect(cdf,random())]
                        words=selectedBigram.split()
                        for word in words:
                            generatedTitle.append(word)

                        if selectedBigram not in GeneratedBigramDistribution:
                            GeneratedBigramDistribution[selectedBigram]=1
                        else:
                            GeneratedBigramDistribution[selectedBigram]+=1
                    #print(generatedTitle)
                    PredictedTitles.append(generatedTitle)


                
                    
                chencherry = SmoothingFunction()
                x=(bleu_score.corpus_bleu(RefereneTitles,PredictedTitles,smoothing_function=chencherry.method3))
                BLEUscores.append(x)
            
            print(itoYear[TrainingEndTime+t],numpy.average(BLEUscores),numpy.std(BLEUscores))
        print('\n\n\n')'''



















        
        
